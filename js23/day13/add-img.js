const inputImg = document.querySelector('#input-img');
    //Xư lý sự kiện khi người dùng thay đổi thông tin bấm chọn vào nút Chọn img
inputImg.addEventListener('change', function(e) {
    //Tuong tác trực tiếp đén đối tượng file dữ liệu

    let file = e.target.file[0] // Truy xuất tên của file
    if (!file) return
    
    let img = document.createElement('img');//Tạo thẻ img
    img.scr = URL.createObjectURL(file);//Lấy nguồn ảnh vừa chọn ở trên gán cho thẻ img

    document.querySelector('.preview').appendChild(img);// Bổ sung thẻ img vào khu vực 
})
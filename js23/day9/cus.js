const customer_name = document.getElementById("name");
const email = document.getElementById("email");
const phone = document.getElementById("phone");
const address = document.getElementById("address");
const addBtn = document.getElementById("add");
const updateBtn = document.getElementById("update");

// Nếu người dùng bấm vào nút ADD => Làm gì:
// 1. Lấy tất cả các thông tin trên form add
// ==> chuyển vào object
// ==> xóa dữ liệu form => đưa ra object localStorage.setItem("customer", object)
// 2. Show list lên table => thì:
// Thêm 1 dòng vào trong list (tbody - table) + data
//  Data => truy xuất từ localStorage

let lists = [];

addBtn.onclick = () => {
    let obj_customer = {
        name: customer_name.value,
        email: email.value,
        phone: phone.value,
        address: address.value,
        
    }
    console.log(obj_customer)
    let getlocalStorageData = localStorage.getItem('customer');
    if (getlocalStorageData == null) {
        lists = [];  // localStorage chưa có phần tử nào
    }else {
        lists = JSON.parse(getlocalStorageData); // Chuyển dữ liệu string từ localStorage thành array
    }
    lists.push(obj_customer); // Đưa dữ liêu từ form (object) vào lists
    localStorage.setItem('customer', JSON.stringify(lists)); // Chuyển lists sang string r đưa dữ liệu vào 
    show_lists(); // Hiển thị ra table
}
function show_lists() {
    let getlocalStorageData = localStorage.getItem('customer');
    if(getlocalStorageData == null) {
        lists =  [];
    }else {
        lists = JSON.parse(getlocalStorageData);
    }
    let row = "";
    lists.forEach((element, index) => {
        row += `
        <tr>
            <td>${index}</td>
            <td>${element.name}</td>
            <td>${element.email}</td>
            <td>${element.phone}</td>
            <td>${element.address}</td>
            <td><button  onclick="edit(${index})">Edit</button></td>
            <td><button class="fas fa-trash" onclick="delete_customer(${index})"></button></td>
        </tr>    
        `
       
    });
    document.getElementById('list').innerHTML = row;
}

function reset_form() {
    customer_name.value = "";
    email.value = "";
    phone.value = "";
    address.value = "";
    customer_name.focus();
}
function delete_customer(index) {
    let getlocalStorageData = localStorage.getItem('customer');
    lists = JSON.parse(getlocalStorageData); // Chuyển dữ liệu từ dàng string sang mảng obj
    lists.splice(index, 1); // Xóa 1 phần tử tại vị trí index
    localStorage.setItem("customer", JSON.stringify(lists));// Cập nhật lại dạng dữ liệu dạng string của localStorage
    show_lists();// Hiển thị lại danh sách trên table 
}

// Hàm edit(index) lấy dữ liệu và đổ lên form
function edit(index) {
    let getlocalStorageData = localStorage.getItem('customer');
    lists = JSON.parse(getlocalStorageData);
    customer_name.value = lists[index].name;
    email.value = lists[index].email;
    phone.value = lists[index].phone;
    address.value = lists[index].address;
    document.getElementById("index").value = index;
}

updateBtn.onclick = () => {
    // Lấy dữ liệu trên forme
    let obj_customer = {
        name: customer_name.value,
        email: email.value,
        phone: phone.value,
        address: address.value,
    }
    let getlocalStorageData = localStorage.getItem('customer');
    lists = JSON.parse(getlocalStorageData);
    // Lấy chỉ số index đê cập nhật phần tử index
    let index = parseInt(document.getElementById("index").value);
    // Cập nhật lại nội dung tại phần tử chỉ số index
    lists[index] = obj_customer;
    // Cập nhật lại localStorage
    localStorage.setItem('customer', JSON.stringify(lists));
    show_lists();// Show lại table    
} 

window.onload = () => {
    show_lists();
}
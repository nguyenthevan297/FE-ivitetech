const btnRegister = document.getElementById("btnRegister");

// binding event handler into button register
btnRegister.addEventListener("click", function () {
    // email
    const emailInput = document.getElementById("emailInput");
    const emailValue = emailInput.value;

    if (emailValue.indexOf("@") === -1) {
        const emailError = document.getElementById("emailError");
        emailError.innerHTML = `
      <div style="color: red;">Invalid Email!!!</div>
    `;
    }

    // phone
    const phoneInput = document.getElementById("phoneInput");
    const phoneValue = phoneInput.value;

    // country
    const countrySelect = document.getElementById("countrySelect");
    const countryValue = countrySelect.value;

    // contact me by
    const listRadios = document.getElementsByName("contactMe");
    let selectedOption = null;

    // loop and find radio checked
    listRadios.forEach((element) => {
        if (element.checked === true) {
            selectedOption = element.value;
        }
    });

    // get termService checkbox
    const termServiceValue = document.getElementById("termService").checked;

    const obj = {
        email: emailValue,
        phone: phoneValue,
        country: countryValue,
        contactMeBy: selectedOption,
        termService: termServiceValue,
    };

    alert(JSON.stringify(obj));
});
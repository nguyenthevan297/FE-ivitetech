function caculateBMI() {
    const heightInput = document.getElementById("height");
    const heightInputValue = heightInput.value;
    const weightInput = document.getElementById("weight");
    const weightInputValue = weightInput.value;

    const result = weightInputValue / Math.pow(heightInputValue, 2);

    alert(`Your BMI : ${result.toFixed(2)}`);
    return result.toFixed(2);
}
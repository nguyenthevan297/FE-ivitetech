"use strict"
let $ = function(id) {
    return document.getElementById(id);
};

function page_num(number) {
    let i = 1;
    let result = "";
    while (i<number) {
        result += "<li><a href='#'>" + i +"</a></li>";
        i++;
    }
    $("result").innerHTML = result;
}


window.onload = function() {
   page_num(10);
}
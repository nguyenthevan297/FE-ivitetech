"use strict"
let $ = function(id) {
    return document.getElementById(id);
};
function caculate () {
    let number1 = parseFloat($("number1").value);
    let number2 = parseFloat($("number2").value);
    let number3 = parseFloat($("number3").value);
    let average = (number1 + number2 + number3) / 3;
    average = average.toFixed(2);
    $("result").innerHTML = rank(average) + "<br>" + message_letter(rank(average)) ;
}
function rank(average) {
    if (average >= 89.5) {
        return "Grade A";
    }else if (average >=79.5) {
        return "Grade B";
    }else if (average >= 69.5) {
        return "Grade C";
    }else if (average >= 64.5) {
        return "Grade D";
    }else {
        return "Grade F";
    }
}
function message_letter(grade) { 
    switch (grade) {
        case "Grade A":
            message = "Well above average";
            break;
        case "Grade B":
            message = "Above average";
            break;
        case "Grade C":
            message = "Average";
            break;
        case "Grade D":
            message = "Below average";
            break;
        case "Grade F":
            message = "Failing";
            break;
        default:
            message = "Invailid grade";
            break;            

    }
}
window.onload = function() {
    $("caculate").onclick = caculate; 
}
var Student = function(name, age, email, phone) {
    this.StudentName = name;
    this.StudentAge = age; 
    this.StudentEmail = email; 
    this.StudentPhone = phone; 
}

Student.prototype.printStudent = function() {
    console.log(this.StudentName, this.StudentAge, this.StudentEmail, this.StudentPhone)
    document.getElementById("list_student").innerHTML += `
        <tr>
            <td>${this.StudentName}</td>
            <td>${this.StudentAge}</td>
            <td>${this.StudentEmail}</td>
            <td>${this.StudentPhone}</td>
        </tr>
    `;
}
Student.prototype.setName = function (name) {
    this.StudentName = name;
}
Student.prototype.setAge = function (age) {
    this.StudentAge = age;
}
Student.prototype.setEmail = function (email) {
    this.StudentEmail = email;
}
Student.prototype.setPhone = function (phone) {
    this.StudentPhone = phone;
}

// Khai báo mảng lưu trữ thông tin sv
var list_student = [];
// Hàm chuyển thông tin người dùng vào
function addStudent() {
    let student = document.querySelectorAll("form input");
    //console.log(student);
    let st = new Student(student[0].value,student[1].value, student[2].value, student[3].value);
    //console.log(st);
    list_student.push(st);
    //console.log(listStudent);
}


function showStudent() {
    let row = '';
    for (let index = 0; index < list_student.length; index++) {
        row += `
        <tr>
        <td>${list_student[index].StudentName}</td>
        <td>${list_student[index].StudentAge}</td>
        <td>${list_student[index].StudentEmail}</td>
        <td>${list_student[index].StudentPhone}</td>
        </tr>
        `;
    }
    document.getElementById("list_student").innerHTML = row;
}

function reset_form() {
    StudentName.value = "";
    StudentAge.value = "";
    StudentEmail.value = "";
    Studentphone.value = "";
    StudentName.focus();
}

const btnSubmit = document.getElementById('add');
btnSubmit.onclick = () => {
    addStudent();
    showStudent();
    reset_form();
}

window.onload = () => {
    showStudent();
}


/* Create new object Student */

/* var st1 = new Student("Nguyen The Vu", 25,"nguyenthevu297@gmail.com", "0971762397" );
var st2 = new Student("Nguyen The V", 40,"nguyentruc297@gmail.com", "0971762397" );
console.log(st1);
st1.printStudent(); */
